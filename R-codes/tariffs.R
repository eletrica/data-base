library(tidyverse)

setwd("~/Documents")

# Functions

# Convert portuguese month names to integer
months_pt <- function(m){
  aux <- c("JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO",
            "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO")
  month_number <- which(aux == m)
  return(as.character(month_number))
}

# Describing values to apply in data analysis
CONCESSIONARIA <- "ENEL CE" # Or choose from agent_list variable after load data
ANO <- 2022
MES <- 3

# Importing data from repositories
URL_ANEEL <- "https://gitlab.com/eletrica/data-base/-/raw/master/tariffs/aneel-2022-05-02.csv"
URL_ENEL <- "https://gitlab.com/eletrica/data-base/-/raw/master/tariffs/params.csv"
URL_DESCS <- "https://gitlab.com/eletrica/data-base/-/raw/master/tariffs/descs.csv"

# Aneel data
dt <- read.csv(file=URL_ANEEL, sep=";", fileEncoding="UTF-16LE", dec=",")
# Enel params
params <- read.csv(file=URL_ENEL, sep=";", fileEncoding="UTF-8", dec=",")
# Descs data
descs <- read.csv(file=URL_DESCS, sep=";", fileEncoding="UTF-8", dec=",", na.strings = c("", "NA"))

# Data processing
agent_list <- distinct(dt, SigAgente)
dt <- dt[order(dt$SigAgente, dt$DatInicioVigencia),]
dt <- filter(dt, SigAgente == CONCESSIONARIA, DscBaseTarifaria == "Tarifa de Aplicação",
             DscModalidadeTarifaria %in% c("Azul", "Verde", "Convencional", "Branca"),
             DscDetalhe == "Não se aplica", SigAgenteAcessante == "Não se aplica") %>%
             separate(DscREH, sep="\\sDE\\s", c("DscREH", "Dia", "Mes", "Ano")) %>%
             mutate(Mes = lapply(Mes, months_pt),
                    DscUnidadeTotal = ifelse(DscUnidadeTerciaria == "R$/MWh", "R$/kWh", DscUnidadeTerciaria),
                    VlrTotal = ifelse(DscUnidadeTerciaria == "R$/MWh", (VlrTUSD+VlrTE)/1000, VlrTUSD+VlrTE)) %>%
             unite(Ano, Mes, Dia, col=DatREH, sep="-") %>% 
             mutate(DscREH = gsub(".*\\s(.*),.*", "\\1", DscREH), .before = DatREH) %>% 
             relocate(SigAgente)
dt$DatREH <- as.Date(dt$DatREH)

# Remove unused columns
dt <- dt %>% select(-DatGeracaoConjuntoDados, -NumCNPJDistribuidora, -SigAgenteAcessante, -DscDetalhe, -DscBaseTarifaria)

reh_list <- select(dt, SigAgente, DscREH, DatREH, DatInicioVigencia, DatFimVigencia) %>% distinct()
res_num <- select(dt, DscREH)[1,]
res_num <- gsub(".*\\s(.*),.*", "\\1", res_num)


media_consumo <- dt %>% group_by(DscREH, DatInicioVigencia) %>%
               filter(DscUnidadeTotal == "R$/kWh") %>%
               summarise("VlrMediaConsumo" = mean(VlrTotal))

media_potencia <- dt %>% group_by(DscREH, DatInicioVigencia) %>%
                filter(DscUnidadeTotal == "R$/kW") %>%
                summarise("VlrMediaPotencia" = mean(VlrTotal))

reh_list <- reh_list %>%
            left_join(media_consumo) %>%
            left_join(., media_potencia)

y <- format(as.Date(str_c(ANO, MES, "01", sep="-")), "%m-%Y")
x <- params %>% filter(format(as.Date(DatFimVigencia), "%m-%Y") == y)
tabela_mes <- dt %>% filter(DscREH == x$DscREH)
tabela_mes <- tabela_mes %>%
  mutate(VlrBandeira = ifelse(DscUnidadeTerciaria == "R$/MWh", round(VlrTotal+x$VlrAcrescimo,5), VlrTotal),
         VlrPISCOFINS = round(VlrBandeira/(1-(x$VlrPISCOFINS/100)), 5),
         VlrICMS = round(VlrPISCOFINS/(1-(x$VlrICMS/100)), 5))
  
tabela_mes <- tabela_mes %>% select(-DatREH, -DatInicioVigencia, -DatFimVigencia)
# filter(as.Date(DatFimVigencia) >= x$DatInicioVigencia & as.Date(DatInicioVigencia) <= x$DatInicioVigencia)

#Save to csv file
OUT_NAME <- str_c(CONCESSIONARIA, ANO, MES, sep="-")
write_csv2(tabela_mes, str_c(OUT_NAME, ".csv"))
